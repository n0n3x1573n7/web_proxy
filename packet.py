from struct import pack, unpack_from

from conversion import *

broadcast_mac = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]
unknown_mac   = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

HTTP_OPS = 'GET POST HEAD PUT DELETE OPTIONS'.split(' ')

def checksum(packet):
    chk=0
    
    for i in range(0, len(msg), 2):
        chk+=(ord(pkt[i])<<8) + (ord(pkt[i+1]))
    
    while s!=(s&0xffff):
        s=(s>>16)+(s&0xffff)

    s=~s&0xffff

    return s

class Packet():
    @classmethod
    def analyze(cls, packet):
        #input: packet, a bytes object
        #output: cls object, a disassembled packet in its object form.
        raise NotImplementedError("analyze must be implemented by its subclasses")

    def get_packet(self):
        #returns a bytes object, a packet to be transmitted.
        raise NotImplementedError("get_packet must be implemented by its subclasses")

    def __len__(self):
        return len(self.get_packet())

class ETH_packet(Packet):
    '''
    !   : Network byte order
    6B  : Destination MAC
    6B  : Source MAC
    H   : Ethertype(ETY)
    '''
    ETHER_FRAME='!6B6BH'

    @classmethod
    def analyze(cls, packet):
        eth_header=unpack_from(cls.ETHER_FRAME, packet)
        self=ETH_packet()

        self.dst_mac=[*(eth_header[0:6])]
        self.src_mac=[*(eth_header[6:12])]

        self.ethertype=eth_header[12]

        self.payload=packet[14:]

        return self

    def __str__(self):
        return 'src MAC: {}\ndst MAC: {}\nethtype: {:04X}\n===PAYLOAD===\n{}'.format(arr_to_mac(self.src_mac), arr_to_mac(self.dst_mac), self.ethertype, self.payload)

    def get_packet(self):
        if isinstance(self.payload, Packet):
            return pack(self.ETHER_FRAME, *self.dst_mac, *self.src_mac, self.ethertype)+self.payload.get_packet()
        return pack(self.ETHER_FRAME, *self.dst_mac, *self.src_mac, self.ethertype)+self.payload

class ARP_packet(Packet):
    '''
    !   : Network byte order
    H   : Hardware Type(HRD)
    H   : Protocol Type(PRO)
    B   : Hardware Addr len(HLN)
    B   : Protocol Addr len(PLN)
    H   : Opcode(1 for request, 2 for response)
    6B  : Sender MAC addr
    4B  : Sender IP addr
    6B  : Target MAC addr
    4B  : Target IP addr
    '''
    ARP_FRAME='!HHBBH6B4B6B4B'
    HRD_BASE=0x0001
    PRO_BASE=0x0800
    HLN_BASE=0x06
    PLN_BASE=0x04

    ETHERTYPE=0x0806

    ARP_REQUEST=0x0001
    ARP_REPLY=0x0002

    @classmethod
    def analyze(cls, packet):
        if isinstance(packet, Packet):
            return packet

        arp_packet=unpack_from(cls.ARP_FRAME, packet)
        self=ARP_packet()
        self.HRD=arp_packet[0]
        self.PRO=arp_packet[1]
        self.HLN=arp_packet[2]
        self.PLN=arp_packet[3]
        self.opcode=arp_packet[4]
        self.sender_mac=arp_packet[5:11]
        self.sender_ip=arp_packet[11:15]
        self.target_mac=arp_packet[15:21]
        self.target_ip=arp_packet[21:25]
        return self

    def __str__(self):
        return 'opcode : {}\nsnd MAC: {}\nsnd IP : {}\ntgt MAC: {} \ntgt IP : {}'.format(self.opcode, arr_to_mac(self.sender_mac), arr_to_ip(self.sender_ip), arr_to_mac(self.target_mac), arr_to_ip(self.target_ip))

    def get_packet(self, *, pad_len=18):
        return pack(self.ARP_FRAME, self.HRD, self.PRO, self.HLN, self.PLN, self.opcode,
                    *self.sender_mac, *self.sender_ip, *self.target_mac, *self.target_ip)+b'\x00'*pad_len#zero padding

class IPv4_packet(Packet):
    '''
    !   : Network Byte Order
    B   : Version, IP Header Length
    B   : Type of Service(DSCP, ECN)
    H   : Total Length
    H   : ID
    H   : Flag, Fragment Offset
    B   : TTL
    B   : protocol
    H   : Header Checksum
    4B  : Source IP
    4B  : Destination IP
    '''
    IP_FRAME='!BBHHHBBH4B4B'
    ETHERTYPE=0x0800
    IPV4=4

    @classmethod
    def analyze(cls, packet):
        if isinstance(packet, Packet):
            return packet
        
        ip_packet=unpack_from(cls.IP_FRAME, packet)
        self=IPv4_packet()
        self.ver, self.ip_hl=ip_packet[0]&0xf0>>4, (ip_packet[0]&0x0f)
        self.DSCP_ECN=ip_packet[1]
        self.totlen=ip_packet[2]
        self.id=ip_packet[3]
        self.flg_fragoff=ip_packet[4]
        self.TTL=ip_packet[5]
        self.protocol=ip_packet[6]
        self.checksum=ip_packet[7]
        self.src_ip=ip_packet[8:12]
        self.dst_ip=ip_packet[12:16]

        self.options=packet[20:self.ip_hl*4]
        self.payload=packet[self.ip_hl*4:]
        
        return self

    def __str__(self):
        return 'src IP: {}\ndst IP: {}\n===PAYLOAD===\n{}'.format(arr_to_ip(self.src_ip), arr_to_ip(self.dst_ip), self.payload)

    def renew_checksum(self):
        self.checksum=0
        pkt=self.get_packet()
        self.checksum=checksum(pkt)

    def get_packet(self):
        self.renew_checksum()
        return pack((self.ver<<4)&self.ip_hl, self.DSCP_ECN, self.totlen, self.id, self.flg_fragoff,
            self.TTL, self.protocol, self.checksum, self.src_ip, self.dst_ip)+self.options\
            +self.payload.get_packet() if isinstance(self.payload, Packet) else self.payload

class TCP_packet(Packet):
    '''
    !   : Network Byte Order
    H   : Source port
    H   : Destination port
    I   : Sequence number
    I   : Acknowledgement number
    H   : Data offset, reserved, flags
    H   : Window size
    H   : Checksum
    H   : Urgent pointer
    '''
    TCP_FRAME='!HHIIHHHH'
    IP_PROTOCOL=0x06

    @classmethod
    def analyze(cls, packet):
        if isinstance(packet, Packet):
            return packet

        tcp_packet=unpack_from(cls.TCP_FRAME, packet)
        self=TCP_packet()
        self.src_port=tcp_packet[0]
        self.dst_port=tcp_packet[1]
        self.seq_num=tcp_packet[2]
        self.ack_num=tcp_packet[3]
        self.data_offset=(tcp_packet[4]&0xf000)>>12
        self.flags=tcp_packet[4]&0x0fff
        self.window_size=tcp_packet[5]
        self.checksum=tcp_packet[6]
        self.urgent=tcp_packet[7]
        self.options=packet[20:self.data_offset*4]
        self.payload=packet[self.data_offset*4:]
        return self

    def __str__(self):
        return 'src port: {}\ndst port: {}\n===  DATA  ===\n{}'.format(self.src_port, self.dst_port, self.payload)

    @property
    def rst(self):
        return (self.flags&0x04)>>2

    @rst.setter
    def rst(self, val:bool):
        return self.flags&0xfa^(0x04*val)

    @property
    def fin(self):
        return self.flags&0x01

    @fin.setter
    def fin(self, val:bool):
        return self.flags&0xfe^(0x01*val)

    def renew_checksum(self):
        self.checksum=0
        pkt=self.get_packet()
        self.checksum=checksum(pkt)

    def get_packet(self):
        self.renew_checksum()
        return pack(self.src_port, self.dst_port, self.seq_nu, self.ack_num, self.data_offset, self.flags,
                    self.window_size, self.checksum, self.urgent)+self.options\
                    +self.payload.get_packet() if isinstance(self.payload, Packet) else self.payload

class HTTP_packet(Packet):
    @classmethod
    def analyze(cls, packet):
        if not HTTP_packet.is_http(packet):
            raise Exception("Given packet is not HTTP")
        self=HTTP_packet()

        self.data=packet

        return self

    def __str__(self):
        return self.plain

    @property
    def data(self):
        return self._data

    @property
    def plain(self):
        return self._plain

    @data.setter
    def data(self, packet):
        self._data=packet
        self._plain=packet.decode().lstrip()

    @plain.setter
    def plain(self, http):
        self._plain=http
        self._data=http.encode()

    @classmethod
    def is_http(cls, packet):
        if not isinstance(packet, str):
            try:
                packet=packet.decode().lstrip()
            except:
                return False
        for op in HTTP_OPS:
            if packet.startswith(op):
                return True
        return False

    def get_host(self):
        try:
            return self.host
        except:
            start=self._plain.find('Host: ')
            if start==-1:
                self.host=None
            else:
                end=self._plain.find('\r\n', start)
                self.host=self._plain[start+len('Host: '):end]
            return self.host

    def get_packet(self):
        return self._data

def analyze_packet(packet):
    eth=ETH_packet.analyze(packet)
    if eth.ethertype==ARP_packet.ETHERTYPE:
        arp=eth.payload=ARP_packet.analyze(eth.payload)
    elif eth.ethertype==IPv4_packet.ETHERTYPE:
        ip=eth.payload=IPv4_packet.analyze(eth.payload)
        if ip.protocol==TCP_packet.IP_PROTOCOL:
            tcp=ip.payload=TCP_packet.analyze(ip.payload)
            if HTTP_packet.is_http(tcp.payload):
                http=tcp.payload=HTTP_packet.analyze(tcp.payload)
    return eth

def get_type(packet):
    if not isinstance(packet, Packet):
        packet=analyze_packet(packet)
    types=['ETH']
    while hasattr(packet, 'payload'):
        packet=packet.payload
        t=repr(type(packet)).split("'")[1].split('.')[1].split('_')[0]
        types.append(t)
    return types

if __name__ == '__main__':
    packet=b'\x00\x00\x0c\x9f\xf1\x46\xa0\xc5\x89\x0f\x7b\xed\x08\x00\x45\x00\x01\x9a\xa0\xa0\x40\x00\x80\x06\xe0\xd8\x0a\x10\x9a\xd8\xaf\xd5\x23\x27\xd6\x97\x00\x50\xd4\xcd\xa8\xa7\xa5\xe1\x33\x88\x50\x18\x02\x01\x64\x9a\x00\x00\x47\x45\x54\x20\x2f\x20\x48\x54\x54\x50\x2f\x31\x2e\x31\x0d\x0a\x48\x6f\x73\x74\x3a\x20\x74\x65\x73\x74\x2e\x67\x69\x6c\x67\x69\x6c\x2e\x6e\x65\x74\x0d\x0a\x55\x73\x65\x72\x2d\x41\x67\x65\x6e\x74\x3a\x20\x4d\x6f\x7a\x69\x6c\x6c\x61\x2f\x35\x2e\x30\x20\x28\x57\x69\x6e\x64\x6f\x77\x73\x20\x4e\x54\x20\x31\x30\x2e\x30\x3b\x20\x57\x69\x6e\x36\x34\x3b\x20\x78\x36\x34\x3b\x20\x72\x76\x3a\x37\x31\x2e\x30\x29\x20\x47\x65\x63\x6b\x6f\x2f\x32\x30\x31\x30\x30\x31\x30\x31\x20\x46\x69\x72\x65\x66\x6f\x78\x2f\x37\x31\x2e\x30\x0d\x0a\x41\x63\x63\x65\x70\x74\x3a\x20\x74\x65\x78\x74\x2f\x68\x74\x6d\x6c\x2c\x61\x70\x70\x6c\x69\x63\x61\x74\x69\x6f\x6e\x2f\x78\x68\x74\x6d\x6c\x2b\x78\x6d\x6c\x2c\x61\x70\x70\x6c\x69\x63\x61\x74\x69\x6f\x6e\x2f\x78\x6d\x6c\x3b\x71\x3d\x30\x2e\x39\x2c\x2a\x2f\x2a\x3b\x71\x3d\x30\x2e\x38\x0d\x0a\x41\x63\x63\x65\x70\x74\x2d\x4c\x61\x6e\x67\x75\x61\x67\x65\x3a\x20\x6b\x6f\x2d\x4b\x52\x2c\x6b\x6f\x3b\x71\x3d\x30\x2e\x38\x2c\x65\x6e\x2d\x55\x53\x3b\x71\x3d\x30\x2e\x37\x2c\x65\x6e\x2d\x47\x42\x3b\x71\x3d\x30\x2e\x35\x2c\x65\x6e\x3b\x71\x3d\x30\x2e\x33\x2c\x65\x6e\x2d\x43\x41\x3b\x71\x3d\x30\x2e\x32\x0d\x0a\x41\x63\x63\x65\x70\x74\x2d\x45\x6e\x63\x6f\x64\x69\x6e\x67\x3a\x20\x67\x7a\x69\x70\x2c\x20\x64\x65\x66\x6c\x61\x74\x65\x0d\x0a\x43\x6f\x6e\x6e\x65\x63\x74\x69\x6f\x6e\x3a\x20\x6b\x65\x65\x70\x2d\x61\x6c\x69\x76\x65\x0d\x0a\x55\x70\x67\x72\x61\x64\x65\x2d\x49\x6e\x73\x65\x63\x75\x72\x65\x2d\x52\x65\x71\x75\x65\x73\x74\x73\x3a\x20\x31\x0d\x0a\x0d\x0a'
    eth=analyze_packet(packet)
    print(get_type(eth))