from proxy import Connection_handler
from sys import argv

def usage():
	print("syntax : python3 web_proxy.py <tcp port>")
	print("sample : python3 web_proxy.py 1234")

def main():
	try:
		name, tcp_port = argv
		tcp_port=int(tcp_port)
	except:
		usage()
		exit()

	Connection_handler(tcp_port).start()


if __name__ == '__main__':
	main()