import socket
import string
import multiprocessing
from hexdump import hexdump
from atexit import register
from packet import analyze_packet

BUFFER_SIZE=4096

def receive_all(conn):
    buf=""
    try:
        while True:
            data = conn.recv(BUFFER_SIZE)
            if not data:
                break
            buf+=data
    except:
        pass
    return buf

class Connection_handler():
    def __init__(self, local_port):
        self.local_port = local_port
        self.socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.procs={}
        register(self.stop)
        try:
            self.socket.bind(("", local_port))
        except Exception as e:
            print(e)
            raise Exception("[!!!] socket failed to bind to {}".format(local_port))

    @classmethod
    def set_pair(self, transmit, receive):
        transmit.pair_queue, receive.pair_queue = receive.queue, transmit.queue

    def start_proc(self, func, addr):
        proc=multiprocessing.Process(target=func)
        if addr not in self.procs:
            self.procs[addr]=None
        self.procs[addr].append(proc)
        proc.start()

    def start(self):
        self.socket.listen(1)

        while True:
            data, addr = sock.recvfrom(BUFFER_SIZE)

            print("[ * ] received connection from {}:{}".format(*addr))

            transmit=client_connection_handler(port, addr)
            receive=remote_connection_handler(port, addr)
            Connection_handler.set_pair(transmit, receive)

            self.start_proc(transmit.start, addr)
            self.start_proc(receive.start, addr)
            self.start_proc(transmit.send_from_queue, addr)
            self.start_proc(receive.send_from_queue, addr)
            print("[ * ] processes started")

    def stop(self):
        self.socket.close()
        for proc in self.procs:
            for pr in self.procs[proc]:
                pr.terminate()

class client_connection_handler():
    def __init__(self, port, addr):
        self.queue=multiprocessing.Queue()
        self.port=port
        self.addr=self.host=addr
        self.pair_queue=None
        self.conn_socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn_socket.bind((addr, port))
        self.socket, addr=self.conn_socket.accept()

    @classmethod
    def handle(cls, data):
        print(hexdump(data))
        return data

    def start(self):
        while True:
            buf=receive_all(self.socket)
            if buf:
                print("[==>] client sends {} byte to {}".format(len(buf), self.host))
                
                buf=client_connection_handler.handle(data)

                self.pair_queue.put(buf)

    def send_from_queue(self):
        while True:
            packet=self.queue.get()
            self.socket.send(buf)
            print("[==>] sent to {}".format(self.host))
            print(hexdump(buf))


class remote_connection_handler():
    def __init__(self, port, addr):
        self.queue=multiprocessing.Queue()
        self.port=port
        self.addr=addr
        self.pair_queue=None
        self.conn_socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn_socket.bind((addr, port))
        self.socket, addr=self.conn_socket.accept()
        self.host=host

    @classmethod
    def handle(cls, data):
        print(hexdump(data))
        return data

    def start(self):
        while True:
            buf=receive_all(self.socket)
            if buf:
                print("[<==] client received {} byte from {}".format(len(buf), self.host))
                
                buf=client_connection_handler.handle(data)

                self.socket.send(buf)
                print("[<==] Sent to {}".format(self.host))

    def send_from_queue(self):
        while True:
            packet=self.queue.get()
            self.socket.send(buf)
            print("[<==] received from {}".format(self.host))
            print(hexdump(buf))

if __name__ == '__main__':
    c=Connection_handler(1234)
    c.start()